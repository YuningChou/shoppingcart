window.onload = function () {
    if (!document.getElementsByClassName) {
        document.getElementsByClassName = function (cls) {
            var ret = [];
            var els = document.getElementsByTagName('*');
            for (var i = 0, len = els.length; i < len; i++) {

                if (els[i].className.indexOf(cls + ' ') >=0 || els[i].className.indexOf(' ' + cls + ' ') >=0 || els[i].className.indexOf(' ' + cls) >=0) {
                    ret.push(els[i]);
                }
            }
            return ret;
        }
    }

    var table = document.getElementById('cartTable'); // 購物車表格
    var selectInputs = document.getElementsByClassName('check'); // 勾選框
    var checkAllInputs = document.getElementsByClassName('check-all') // 全選框
    var tr = table.children[1].rows; //行
    var selectedTotal = document.getElementById('selectedTotal'); //已選商品目錄
    var priceTotal = document.getElementById('priceTotal'); //總計
    var deleteAll = document.getElementById('deleteAll'); // 刪除全部
    var selectedViewList = document.getElementById('selectedViewList'); //已選商品目錄
    var selected = document.getElementById('selected'); //已選商品
    var foot = document.getElementById('foot');

    // 更新總數與總價
    function getTotal() {
        var selected = 0, price = 0, html = '';
        for (var i = 0; i < tr.length; i++) {
            if (tr[i].getElementsByTagName('input')[0].checked) {
                tr[i].className = 'on';
                selected += parseInt(tr[i].getElementsByTagName('input')[1].value); //計算已選商品
                price += parseFloat(tr[i].getElementsByTagName('td')[4].innerHTML); //計算總價
                html += '<div><img src="'+tr[i].getElementsByTagName('img')[0].src+'"><span class="del" index="'+i+'">取消</span></div>';
            }else{
                tr[i].className = '';
            }
        }
        selectedTotal.innerHTML = selected; // 已選數目
        priceTotal.innerHTML = price.toFixed(2); // 總價
        selectedViewList.innerHTML = html;
        if (selected==0) {
            foot.className = 'foot';
        }
    }

    // 計算單品價格
    function getSubtotal(tr) {
        var cells = tr.cells;
        var price = cells[2]; //單價
        var subtotal = cells[4]; //小計
        var countInput = tr.getElementsByTagName('input')[1]; //數目
        var span = tr.getElementsByTagName('span')[1]; //-減
        //寫入HTML
        subtotal.innerHTML = (parseInt(countInput.value) * parseFloat(price.innerHTML)).toFixed(2);
        //如果數量只有一個,就不要show減號
        if (countInput.value == 1) {
            span.innerHTML = '';
        }else{
            span.innerHTML = '-';
        }
    }

    // 點擊選擇框
    for(var i = 0; i < selectInputs.length; i++ ){
        selectInputs[i].onclick = function () {
            if (this.className.indexOf('check-all') >= 0) { //如果是全選，把全部選擇框選取
                for (var j = 0; j < selectInputs.length; j++) {
                    selectInputs[j].checked = this.checked;
                }
            }
            if (!this.checked) { //只要有一個沒選，就取消全選框的選取狀態
                for (var i = 0; i < checkAllInputs.length; i++) {
                    checkAllInputs[i].checked = false;
                }
            }
            getTotal();//選完更新總計
        }
    }

    // 顯示已選商品
    selected.onclick = function () {
        if (selectedTotal.innerHTML != 0) {
            foot.className = (foot.className == 'foot' ? 'foot show' : 'foot');
        }
    }

    //已選商品中的取消按鈕
    selectedViewList.onclick = function (e) {
        var e = e || window.event;
        var el = e.srcElement;
        if (el.className=='del') {
            var input =  tr[el.getAttribute('index')].getElementsByTagName('input')[0]
            input.checked = false;
            input.onclick();
        }
    }

    
    for (var i = 0; i < tr.length; i++) {
        tr[i].onclick = function (e) {
            var e = e || window.event;
            var el = e.target || e.srcElement; //事件對象的target獲取觸發元素
            var cls = el.className; //觸發元素的class
            var countInout = this.getElementsByTagName('input')[1]; // 數目input
            var value = parseInt(countInout.value); //數目
            //通過判斷觸發元素的class確定用戶點擊哪個
            switch (cls) {
                case 'add': //點擊+
                    countInout.value = value + 1;
                    getSubtotal(this);
                    break;
                case 'reduce': //點擊-
                    if (value > 1) {
                        countInout.value = value - 1;
                        getSubtotal(this);
                    }
                    break;
                case 'delete': //點擊删除
                    var conf = confirm('確定刪除此商品嗎？');
                    if (conf) {
                        this.parentNode.removeChild(this);
                    }
                    break;
            }
            getTotal();
        }
        
        tr[i].getElementsByTagName('input')[1].onkeyup = function () {
            var val = parseInt(this.value);
            if (isNaN(val) || val <= 0) {
                val = 1;
            }
            if (this.value != val) {
                this.value = val;
            }
            getSubtotal(this.parentNode.parentNode); //更新小計
            getTotal(); //更新總數
        }
    }

    // 點擊全部刪除
    deleteAll.onclick = function () {
        if (selectedTotal.innerHTML != 0) {
            var con = confirm('確定刪除全部商品嗎？'); //彈出確認框
            if (con) {
                for (var i = 0; i < tr.length; i++) {
                    // 如果選了,就刪除相應的
                    if (tr[i].getElementsByTagName('input')[0].checked) {
                        tr[i].parentNode.removeChild(tr[i]); //刪除相應節點
                        i--; //回退
                    }
                }
            }
        } else {
            alert('請選擇商品！');
        }
        getTotal(); //更新總數
    }

    // 默認全選
    checkAllInputs[0].checked = false;
    checkAllInputs[0].onclick();
}
